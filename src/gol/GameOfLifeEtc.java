package gol;
import java.awt.image.BufferedImage;


public class GameOfLifeEtc {



  public static BufferedImage golBufferToImage(GameOfLifeBuffer golBuffer) {
    BufferedImage image = new BufferedImage(golBuffer.getWidth(), golBuffer.getHeight(), BufferedImage.TYPE_INT_ARGB);
    for (int x = 0; x < golBuffer.getWidth(); x++) {
      for (int y = 0; y < golBuffer.getHeight(); y++) {
        int r = 0;
        int g = 0;
        int b = 0;
        int a = 255;
        if (golBuffer.getCell(x, y) == 1) {
          r = 255;
          g = 255;
          b = 255;
        }
        image.setRGB(x, y, (a << 24) | (r << 16) | (g << 8) | b);
      }
    }
    return image;
  }

  public static GameOfLifeBuffer imageToGolBuffer(BufferedImage image) {
    GameOfLifeBuffer golBuffer = new GameOfLifeBuffer(image.getWidth(), image.getHeight());
    for (int x = 0; x < image.getWidth(); x++) {
      for (int y = 0; y < image.getHeight(); y++) {
        int color = -image.getRGB(x, y);
        if (color < 16777216) {
          golBuffer.setCell(x, y, 1);
        }
      }
    }
    return golBuffer;
  }
}
