package utils;

public class LoopTimer {
  private final float delay;
  private long lastPerformAt;

  public LoopTimer(float delay) {
    this.delay = delay;
    markPerform();
  }

  public boolean shouldPerform() {
    return System.currentTimeMillis() - lastPerformAt >= delay;
  }

  public boolean shouldPerform(boolean autoMark) {
    boolean result = shouldPerform();
    if (result && autoMark) {
      markPerform();
    }
    return result;
  }

  public void markPerform() {
    lastPerformAt = System.currentTimeMillis();
  }

  public float getDelay() {
    return delay;
  }
}
