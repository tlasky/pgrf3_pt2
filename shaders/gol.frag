#version 420
out vec4 outColor;

uniform int cellState;
uniform vec2 cellPosition;
uniform vec2 cursorPosition;
uniform vec2 structureSize;
uniform bool isInputMode;

void main() {
    // Floor it so it can be compared.
    vec2 cellPosition = floor(cellPosition);
    vec2 cursorPosition = floor(cursorPosition);
    vec2 structureSize = floor(structureSize);

    // Box around a structure.
    vec2 structureLU = cursorPosition;
    vec2 structureRD = floor(cursorPosition + structureSize - vec2(1));
    vec2 structureRU = vec2(structureRD.x, structureLU.y);
    vec2 structureLL = vec2(structureLU.x, structureRD.y);

    bool isStructureCorner = (
    cellPosition == structureLU || cellPosition == structureRD ||
    cellPosition == structureRU || cellPosition == structureLL
    );
    bool isStructureOver = (
    cellPosition.x >= structureLU.x && cellPosition.x <= structureRD.x &&
    cellPosition.y >= structureLU.y && cellPosition.y <= structureRD.y
    );

    if (isInputMode && isStructureCorner) {
        outColor = vec4(1, 0, cellState, 1);
    } else if (isInputMode && isStructureOver){
        outColor = vec4(0, 1, cellState, 1);
    } else {
        outColor = vec4(vec3(cellState), 1);
    }
}
