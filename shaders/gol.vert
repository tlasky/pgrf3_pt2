#version 420
in vec2 inPosition;

uniform vec2 gameSize;
uniform vec2 cellPosition;
uniform vec2 cursorPosition;

void main() {
    vec2 position = inPosition.xy;

    // Scale
    position *=  (1 / gameSize) * 2;

    // Position
    position.x += -1 + (cellPosition.x / gameSize.x) * 2;
    position.y +=  1 - (cellPosition.y / gameSize.y) * 2 - (1 / gameSize.y) * 2;

    gl_Position = vec4(position, 0, 1);
}
