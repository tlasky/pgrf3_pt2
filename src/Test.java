import gol.GameOfLife;
import gol.GameOfLifeBuffer;
import gol.GameOfLifeStructures;


public class Test {
  public static void main(String[] args) {
    /*String textData = """
        .....*.....
        .....*.....
        .....*.....
        """;
    GameOfLifeBuffer buffer = GameOfLifeBuffer.parseText(textData, '*', '.');*/
    GameOfLifeBuffer buffer = new GameOfLifeBuffer(10, 10);
    GameOfLife gof = new GameOfLife(buffer);

    gof.addStructure(GameOfLifeStructures.glider, 5, 5);
    //gof.addStructure(GameOfLifeStructures.block, 5, 5);
    //gof.addStructure(GameOfLifeStructures.glider, 0, 0);
    //gof.addStructure(GameOfLifeStructures.hugeCircle, 5, 5);
    //gof.randomize(50);
    //gof.addStructure(GameOfLifeStructures.glider, 50, 2);

    //while (true) {
    for (int i = 0; i < 100; i++) {
      System.out.println(gof.getCurrentGeneration().toPrint());
      System.out.println();
      gof.step();
    }
  }
}
