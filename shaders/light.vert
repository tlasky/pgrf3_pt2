#version 420
in vec3 inPosition;
in vec3 inTexCoord;
in vec3 inNormal;

uniform int solid;
uniform mat4 view;
uniform mat4 projection;
uniform float time;

const float PI = 3.141592653589793;

mat3 rotation3dZ(float angle) {
    // https://github.com/dmnsgn/glsl-rotate
    // rotation-3d-*osa*.glsl

    float s = sin(angle);
    float c = cos(angle);

    return mat3(
    c, s, 0.0,
    -s, c, 0.0,
    0.0, 0.0, 1.0
    );
}

mat3 rotation3dY(float angle) {
    float s = sin(angle);
    float c = cos(angle);

    return mat3(
    c, 0.0, -s,
    0.0, 1.0, 0.0,
    s, 0.0, c
    );
}

mat3 rotation3dX(float angle) {
    float s = sin(angle);
    float c = cos(angle);

    return mat3(
    1.0, 0.0, 0.0,
    0.0, c, s,
    0.0, -s, c
    );
}

vec3 getPlane(vec2 pos) {
    return vec3(pos, -1);
}

vec3 getSphere(vec2 pos, float r, float a, float b) {
    float az = pos.x * PI;
    float ze = pos.y * PI / 2;

    float x = r * cos(az) * cos(ze);
    float y = a * r * sin(az) * cos(ze);
    float z = b * r * sin(ze);

    return vec3(x, y, z);
}

vec3 getBoobs(vec2 pos) {
    // Tohle těleso by se mělo počítat za 2.
    int m = 1000;
    int n = 4;
    float o = 0.1;

    float x = pos.x * 10;
    float y = pos.y * 10;
    float z =
    exp(-pow(pow(x - n, 2) + pow(y - n, 2), 2) / m) +
    exp(-pow(pow(x + n, 2) + pow(y + n, 2), 2) / m) +
    o * exp(-pow(pow(x + n, 2) + pow(y + n, 2), 2)) +
    o * exp(-pow(pow(x - n, 2) + pow(y - n, 2), 2));

    return vec3(x / 10, y / 10, z);
}

vec3 getCylinder(vec2 pos) {
    float az = pos.x * PI;
    float r = 1;

    float x = r*cos(az);
    float y = r*sin(az);
    float z = pos.y * 2;
    return vec3(x, y, z);
}

void main() {
    vec3 position = inPosition * 2 - 1;

    vec3 scale = vec3(1);
    vec3 move = vec3(0);

    if (solid == 0) {
        scale = vec3(0.5);
        move = vec3(0, -6, -1);

        position = inPosition * rotation3dZ(1.5708) * rotation3dY(1.5708 * 2) * rotation3dX(time * 0.7) * rotation3dY(time * 0.5);
    } else if (solid == 1) {
        scale = vec3(3);

        position = getPlane(position.xy) * rotation3dZ(-1.5708);
    } else if (solid == 2) {
        scale = vec3(0.7);
        move = vec3(1, 1, 0);

        position = getSphere(position.xy, 1, 2, 0.5) * rotation3dZ(time);
    } else if (solid == 3) {
        scale = vec3(3);
        move = vec3(-6, 0, 0);

        position = getBoobs(position.xy) * rotation3dZ(-0.785398) * rotation3dY(-1.5708);
    } else if (solid == 4) {
        scale = vec3(2);
        move = vec3(0, 6, -3);

        position = getCylinder(position.xy) * rotation3dZ(-time);
    } else if (solid == 5) {
        position = vec3(1);
    }

    position *= scale;
    position.x += move.x;
    position.y += move.y;
    position.z += move.z;

    gl_Position = projection * view * vec4(position, 1.0);
}
