package gol

import kotlin.random.Random

class GameOfLifeBuffer(val width: Int, val height: Int) {
    /* Important!
        This buffer will wrap around itself. ==> [-1, -1] becomes [width, height] and etc...
     */
    val buffer = Array(width) { Array(height) { 0 } }

    fun getCell(x: Int, y: Int): Int {
        val xIndex = (x + 1) % width
        val yIndex = (y + 1) % height
        return buffer[xIndex][yIndex]
    }

    fun setCell(x: Int, y: Int, cell: Int) {
        val xIndex = (x + 1) % width
        val yIndex = (y + 1) % height
        buffer[xIndex][yIndex] = cell
    }

    fun toPrint(): String {
        var result = ""
        for (y in 0 until height) {
            for (x in 0 until width) {
                result += if (getCell(x, y) == 1) "█" else "░"
            }
            if (y < height - 1) {
                result += "\n"
            }
        }
        return result
    }
}


class GameOfLife(buffer: GameOfLifeBuffer) {
    var currentGeneration = buffer

    fun reset() {
        currentGeneration = GameOfLifeBuffer(currentGeneration.width, currentGeneration.height)
    }

    fun step() {
        currentGeneration = generateNextGeneration()
    }

    fun randomize(minChance: Int = 50) {
        for (x in 0 until currentGeneration.width) {
            for (y in 0 until currentGeneration.height) {
                currentGeneration.setCell(x, y, if (Random.nextInt() * 100 >= minChance) 1 else 0)
            }
        }
    }

    fun addStructure(structure: Array<Array<Int>>, x: Int = 0, y: Int = 0) {
        for (structureY in structure.indices) {
            for (structureX in structure[structureY].indices) {
                // Structure y is first!
                currentGeneration.setCell(x + structureX, y + structureY, structure[structureY][structureX])
            }
        }
    }

    private fun generateNextGeneration(): GameOfLifeBuffer {
        val nextGeneration = GameOfLifeBuffer(currentGeneration.width, currentGeneration.height)
        for (x in 0 until currentGeneration.width) {
            for (y in 0 until currentGeneration.height) {
                // Count neighbors
                var aliveNeighbors = 0;
                for (i in 1 downTo -1) {
                    for (j in 1 downTo -1) {
                        aliveNeighbors += currentGeneration.getCell(x + i, y + j)
                    }
                }
                aliveNeighbors -= currentGeneration.getCell(x, y)

                /* Rules of life
                    Any live cell with fewer than two live neighbours dies, as if by underpopulation.
                    Any live cell with two or three live neighbours lives on to the next generation.
                    Any live cell with more than three live neighbours dies, as if by overpopulation.
                    Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
                 */
                var cellState = currentGeneration.getCell(x, y)
                if ((cellState == 1) and (aliveNeighbors < 2)) {
                    cellState = 0
                } else if ((cellState == 1) and (aliveNeighbors <= 3)) {
                    cellState = 1
                } else if ((cellState == 1) and (aliveNeighbors > 3)) {
                    cellState = 0
                } else if ((cellState == 0) and (aliveNeighbors == 3)) {
                    cellState = 1
                }
                nextGeneration.setCell(x, y, cellState)
            }
        }
        return nextGeneration
    }
}
