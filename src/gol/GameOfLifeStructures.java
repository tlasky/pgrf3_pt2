package gol;


import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class GameOfLifeStructures {
    /* Important!
        Every structure is coded by y then x!
   */

  public static Integer[][] reverseX(Integer[][] structure) {
    Integer[][] buffer = new Integer[structure.length][structure[0].length];
    for (int y = 0; y < structure.length; y++) {
      Integer[] line = new Integer[structure[y].length];
      for (int x = 0; x < structure[y].length; x++) {
        line[x] = structure[y][structure[y].length - 1 - x];
      }
      buffer[y] = line;
    }
    return buffer;
  }

  public static Integer[][] reverseY(Integer[][] structure) {
    Integer[][] buffer = new Integer[structure.length][structure[0].length];
    for (int y = 0; y < structure.length; y++) {
      buffer[y] = structure[structure.length - 1 - y];
    }
    return buffer;
  }

  public static Integer[][] parseText(String text, Character live, Character death) {
    int width = 0;
    int height = 0;
    String trimmedText = "";
    for (String line : text.trim().split("\n")) { // Create trimmedText, compute width and height
      String trimmedLine = "";
      for (Character character : line.trim().toCharArray()) {
        if ((live.toString() + death.toString()).contains(character.toString())) {
          trimmedLine += character.toString();
        } else {
          break;
        }
      }
      if (!trimmedLine.isEmpty()) {
        width = Math.max(width, trimmedLine.length());
        height += 1;
        trimmedText += trimmedLine + "\n";
      }
    }

    Integer[][] buffer = new Integer[height][width];
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        if (x < trimmedText.split("\n")[y].length()) {
          Character cellChar = trimmedText.split("\n")[y].toCharArray()[x];
          buffer[y][x] = cellChar.equals(live) ? 1 : 0;
        } else {
          buffer[y][x] = 0;
        }
      }
    }
    return buffer;
  }

  public static Integer[][] parseTextFile(String path, Character live, Character death) {
    String fileContent = "";
    try {
      fileContent = Files.readString(Paths.get(path));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return parseText(fileContent, live, death);
  }

  public static int structureHeight(Integer[][] structure) {
    return structure.length;
  }

  public static int structureWidth(Integer[][] structure) {
    int width = 0;
    for (Integer[] line : structure) {
      width = Math.max(width, line.length);
    }
    return width;
  }

  public static float[] structureSize(Integer[][] structure) {
    return new float[]{
        structureWidth(structure),
        structureHeight(structure)
    };
  }

  public static Map<String, Integer[][]> getAllStructures() {
    Map<String, Integer[][]> structures = new HashMap<>();
    for (Field filed : GameOfLifeStructures.class.getFields()) {
      try {
        structures.put(filed.getName(), (Integer[][]) filed.get(GameOfLifeStructures.class));
      } catch (Exception ignored) {
      }
    }
    return structures;
  }

  // Building
  public static final Integer[][] erase = {{0}};
  public static final Integer[][] point = {{1}};

  // Still
  public static final Integer[][] block = {
      {1, 1},
      {1, 1}
  };
  public static final Integer[][] beeHive = {
      {0, 1, 1, 0},
      {1, 0, 0, 1},
      {0, 1, 1, 0}
  };

  // Oscillators
  public static final Integer[][] blinker = {
      {0, 1, 0},
      {0, 1, 0},
      {0, 1, 0}
  };
  public static final Integer[][] hugeCircle = {
      {0, 1, 0},
      {1, 1, 1}
  };
  public static final Integer[][] cloverLeaf = {
      {0, 0, 0, 1, 0, 1, 0, 0, 0},
      {0, 1, 1, 1, 0, 1, 1, 1, 0},
      {1, 0, 0, 0, 1, 0, 0, 0, 1},
      {1, 0, 1, 0, 0, 0, 1, 0, 1},
      {0, 1, 1, 0, 1, 0, 1, 1, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0}, // <== Center
      {0, 1, 1, 0, 1, 0, 1, 1, 0},
      {1, 0, 1, 0, 0, 0, 1, 0, 1},
      {1, 0, 0, 0, 1, 0, 0, 0, 1},
      {0, 1, 1, 1, 0, 1, 1, 1, 0},
      {0, 0, 0, 1, 0, 1, 0, 0, 0},
  };

  // Spaceships
  public static final Integer[][] glider = {
      {0, 1, 0},
      {0, 0, 1},
      {1, 1, 1}
  };

  public static final Integer[][] gliderRX = reverseX(glider);
  public static final Integer[][] gliderRY = reverseY(glider);
  public static final Integer[][] gliderRXY = reverseY(reverseX(glider));
  public static final Integer[][] gliderRYX = reverseX(reverseY(glider));

  // Patterns
  public static final Integer[][] gliderFactory = parseTextFile("res/patterns/gliderFactory.txt", 'X', '-');
  public static final Integer[][] spiral = parseTextFile("res/patterns/spiral.txt", 'X', '-');
  public static final Integer[][] fish = parseTextFile("res/patterns/fish.txt", 'X', '-');
  public static final Integer[][] david = parseTextFile("res/patterns/david.txt", 'o', '_');
  public static final Integer[][] pgrf = parseTextFile("res/patterns/pgrf.txt", 'o', '_');

  public static final Integer[][] gliderFactoryRX = reverseX(gliderFactory);
  public static final Integer[][] gliderFactoryRY = reverseY(gliderFactory);
  public static final Integer[][] gliderFactoryRXY = reverseY(reverseX(gliderFactory));
  public static final Integer[][] gliderFactoryRYX = reverseX(reverseY(gliderFactory));
}
