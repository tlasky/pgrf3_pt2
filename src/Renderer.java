import gol.GameOfLife;
import gol.GameOfLifeBuffer;
import gol.GameOfLifeEtc;
import gol.GameOfLifeStructures;
import lwjglutils.*;
import org.lwjgl.glfw.*;
import transforms.*;
import utils.GridFactory;
import utils.LoopTimer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30.glBindFramebuffer;

/**
 * @author David Tláskal
 * @version 2.0
 * @since 2021-12-08
 */
public class Renderer extends AbstractRenderer {

  private double currentMx, currentMy, oldMx, oldMy;
  private boolean mouseLeft, mouseRight;

  private int golShader, lightShader, viewShader;

  private Camera camera, lightCamera;
  private OGLBuffers golPlane;
  private GameOfLife gol;
  private LoopTimer golStepTimer;
  private OGLRenderTarget golTarget, lightTarget;
  private OGLTexture2D.Viewer textureViewer;
  private OGLTextRenderer textRenderer;
  private OGLModelOBJ viewModel;
  private OGLBuffers viwPlane;

  private Mat4 projection;

  private int locGameSize, locCellPosition, locCellState, locCursorPosition, locIsInputMode, locStructureSize;
  private int locProjection, locView, locSolid, locTime, locLightViewProjection, locLightPosition, locEyePosition, locUseLight;
  private int locLightProjection, locLightView, locLightSolid, locLightTime;

  private int polygonMode = GL_FILL;
  private final List<Integer> polygonModes = Arrays.asList(
      GL_FILL,
      GL_LINE
  );
  private String windowMode = "display";
  private final List<String> windowModes = Arrays.asList(
      "display",
      "input"
  );
  private String golStructure;
  private final Map<String, Integer[][]> golStructures = GameOfLifeStructures.getAllStructures();
  private boolean shouldStep = false;
  private float time = 0;
  private boolean useLight = true;
  private final List<String> messages = new ArrayList<>();

  @Override
  public void init() {
    OGLUtils.printOGLparameters();
    OGLUtils.printJAVAparameters();
    OGLUtils.printLWJLparameters();
    OGLUtils.shaderCheck();

    camera = new Camera()
        .withPosition(new Vec3D(8, -1, 4))
        .withAzimuth(3.15)
        .withZenith(-0.65);
    lightCamera = new Camera()
        .withPosition(new Vec3D(1, 1, 4))
        .withAzimuth(4)
        .withZenith(-1.58);

    golShader = ShaderUtils.loadProgram("/gol");
    viewShader = ShaderUtils.loadProgram("/view");
    lightShader = ShaderUtils.loadProgram("/light");
    golPlane = GridFactory.generateTriangleListGrid(2, 2);
    golStepTimer = new LoopTimer(50);
    golTarget = new OGLRenderTarget(1024, 1024);
    textureViewer = new OGLTexture2D.Viewer();
    gol = new GameOfLife(new GameOfLifeBuffer(150, 150));
    projection = new Mat4PerspRH(Math.PI / 3, 600 / 800f, 1.0, 20.0);
    viewModel = new OGLModelOBJ("/models/Vase.obj");
    viwPlane = GridFactory.generateTriangleListGrid(100, 100);
    lightTarget = new OGLRenderTarget(1024, 1024);
    textRenderer = new OGLTextRenderer(width, height);

    gol.addStructure(
        GameOfLifeStructures.david,
        gol.getCurrentGeneration().getWidth() / 2 - 30,
        gol.getCurrentGeneration().getHeight() / 2
    );
    gol.addStructure(
        GameOfLifeStructures.pgrf,
        gol.getCurrentGeneration().getWidth() / 2 - 30,
        gol.getCurrentGeneration().getHeight() / 2 + 10
    );

    locGameSize = glGetUniformLocation(golShader, "gameSize");
    locCellPosition = glGetUniformLocation(golShader, "cellPosition");
    locCellState = glGetUniformLocation(golShader, "cellState");
    locCursorPosition = glGetUniformLocation(golShader, "cursorPosition");
    locIsInputMode = glGetUniformLocation(golShader, "isInputMode");
    locStructureSize = glGetUniformLocation(golShader, "structureSize");

    locProjection = glGetUniformLocation(viewShader, "projection");
    locView = glGetUniformLocation(viewShader, "view");
    locSolid = glGetUniformLocation(viewShader, "solid");
    locTime = glGetUniformLocation(viewShader, "time");
    locLightPosition = glGetUniformLocation(viewShader, "lightPosition");
    locEyePosition = glGetUniformLocation(viewShader, "eyePosition");
    locLightViewProjection = glGetUniformLocation(viewShader, "lightViewProjection");
    locUseLight = glGetUniformLocation(viewShader, "useLight");

    locLightProjection = glGetUniformLocation(lightShader, "projection");
    locLightView = glGetUniformLocation(lightShader, "view");
    locLightSolid = glGetUniformLocation(lightShader, "solid");
    locLightTime = glGetUniformLocation(lightShader, "time");

    golStructures.remove("erase");
    golStructure = "point";
  }

  @Override
  public void display() {
    update();
    glEnable(GL_DEPTH_TEST);
    renderGame();
    if (windowMode.equals("display")) {
      if (useLight) {
        renderLight();
      }
      renderView();
    }
    renderText();
  }

  public void renderGame() {
    glUseProgram(golShader);

    if (windowMode.equals("input")) {
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
      glViewport(0, 0, width, height);
    } else {
      golTarget.bind();
    }

    glClearColor(1f, 1f, 1f, 1f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPolygonMode(GL_FRONT_AND_BACK, polygonMode); // Rendering

    glUniform2f(locGameSize, gol.getCurrentGeneration().getWidth(), gol.getCurrentGeneration().getHeight());
    glUniform2fv(locCursorPosition, getGolCursorPosition());
    glUniform1i(locIsInputMode, windowMode.equals("input") ? 1 : 0);
    glUniform2fv(locStructureSize, GameOfLifeStructures.structureSize(golStructures.get(golStructure)));

    for (int x = 0; x < gol.getCurrentGeneration().getWidth(); x++) {
      for (int y = 0; y < gol.getCurrentGeneration().getHeight(); y++) {
        glUniform2f(locCellPosition, x, y);
        glUniform1i(locCellState, gol.getCurrentGeneration().getCell(x, y));

        golPlane.draw(GL_TRIANGLES, golShader);
      }
    }

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); // Rendering end
  }

  public void renderLight() {
    glUseProgram(lightShader);

    lightTarget.bind();

    glClearColor(1f, 0f, 0f, 1f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPolygonMode(GL_FRONT_AND_BACK, polygonMode); // Rendering

    glUniformMatrix4fv(locLightView, false, lightCamera.getViewMatrix().floatArray());
    glUniformMatrix4fv(locLightProjection, false, projection.floatArray());
    glUniform1f(locLightTime, time);

    glUniform1i(locLightSolid, 0);
    viewModel.getBuffers().draw(viewModel.getTopology(), lightShader);

    for (int i = 1; i <= 5; i++) {
      glUniform1i(locLightSolid, i);
      viwPlane.draw(GL_TRIANGLES, lightShader);
    }

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); // Rendering end
  }

  private void renderView() {
    glUseProgram(viewShader);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, width, height);

    glClearColor(0f, 0f, 0f, 1f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPolygonMode(GL_FRONT_AND_BACK, polygonMode); // Rendering

    glUniformMatrix4fv(locView, false, camera.getViewMatrix().floatArray());
    glUniformMatrix4fv(locProjection, false, projection.floatArray());
    glUniformMatrix4fv(locLightViewProjection, false, lightCamera.getViewMatrix().mul(projection).floatArray());
    glUniform1f(locTime, time);
    glUniform1f(locUseLight, useLight ? 1 : 0);
    glUniform3fv(locLightPosition, ToFloatArray.convert(lightCamera.getPosition()));
    glUniform3fv(locEyePosition, ToFloatArray.convert(camera.getEye()));
    golTarget.getColorTexture().bind(viewShader, "golTexture", 1);
    lightTarget.getDepthTexture().bind(viewShader, "depthTexture", 2);

    glUniform1i(locSolid, 0);
    viewModel.getBuffers().draw(viewModel.getTopology(), viewShader);

    for (int i = 1; i <= 5; i++) {
      glUniform1i(locSolid, i);
      viwPlane.draw(GL_TRIANGLES, viewShader);
    }

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); // Rendering end

    float scale = 0.3f;
    textureViewer.view(golTarget.getColorTexture(), -1, -1, scale);
    if (useLight) {
      textureViewer.view(lightTarget.getDepthTexture(), -1 + scale, -1, scale);
      textureViewer.view(lightTarget.getColorTexture(), -1 + scale * 2, -1, scale);
    }
  }

  private void writeMessage(String message) {
    messages.addAll(Arrays.asList(message.split("\n")));
  }

  private void renderText() {
    writeMessage(String.format(
        "Window mode: %s%nRunning: %s%nUsing light: %s",
        windowMode,
        shouldStep,
        useLight
    ));
    if (windowMode.equals("input")) {
      writeMessage(String.format("Selected structure: %s", golStructure));
      float[] cursorPosition = getGolCursorPosition();
      writeMessage(String.format(
          "Cursor position: [%s, %s]",
          (int) cursorPosition[0] + 1,
          (int) cursorPosition[1] + 1
      ));
      writeMessage(String.format(
          "Structure size: [%s, %s]",
          GameOfLifeStructures.structureWidth(golStructures.get(golStructure)),
          GameOfLifeStructures.structureHeight(golStructures.get(golStructure))
      ));
    }

    int y = 0;
    int yIncrement = 15;
    for (String message : messages) {
      y += yIncrement;
      textRenderer.addStr2D(0, y, message);
    }
  }

  private void update() {
    messages.clear();
    time += 0.01;
    if (golStepTimer.shouldPerform(true) && shouldStep) {
      gol.step();
    }

    if (Math.sin(time) < 0) {
      lightCamera = lightCamera.right(0.01);
    } else {
      lightCamera = lightCamera.left(0.01);
    }
    if (Math.cos(time) < 0) {
      lightCamera = lightCamera.forward(0.01);
    } else {
      lightCamera = lightCamera.backward(0.01);
    }
  }

  @Override
  public GLFWCursorPosCallback getCursorCallback() {
    return cursorPosCallback;
  }

  @Override
  public GLFWMouseButtonCallback getMouseCallback() {
    return mouseButtonCallback;
  }

  @Override
  public GLFWKeyCallback getKeyCallback() {
    return keyCallback;
  }

  @Override
  public GLFWWindowSizeCallback getWsCallback() {
    return wsCallback;
  }

  @Override
  public GLFWScrollCallback getScrollCallback() {
    return scrollCallback;
  }

  private GLFWWindowSizeCallback wsCallback = new GLFWWindowSizeCallback() {
    @Override
    public void invoke(long window, int w, int h) {
      if (w > 0 && h > 0) {
        width = w;
        height = h;
        if (textRenderer != null) {
          textRenderer.resize(width, height);
        }
      }
    }
  };

  private GLFWScrollCallback scrollCallback = new GLFWScrollCallback() {
    @Override
    public void invoke(long window, double dx, double dy) {
      List<String> allStructures = golStructures.keySet().stream().sorted().toList();
      golStructure = allStructures.get(
          Math.abs((allStructures.indexOf(golStructure) - (int) dy)) % allStructures.size()
      );
    }
  };

  private final GLFWCursorPosCallback cursorPosCallback = new GLFWCursorPosCallback() {
    @Override
    public void invoke(long window, double x, double y) {
      currentMx = x;
      currentMy = y;
      if (windowMode.equals("display") && mouseLeft) {
        camera = camera.addAzimuth(Math.PI * (oldMx - x) / LwjglWindow.WIDTH);
        camera = camera.addZenith(Math.PI * (oldMy - y) / LwjglWindow.HEIGHT);
        oldMx = x;
        oldMy = y;
      }
    }
  };

  double map(double x, double inMin, double inMax, double outMin, double outMax) {
    return (x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
  }

  float[] getGolCursorPosition() {
    // Map cursor location to GOL
    return new float[]{
        (float) map(currentMx, 0, width, 0, gol.getCurrentGeneration().getWidth()),
        (float) map(currentMy, 0, height, 0, gol.getCurrentGeneration().getHeight())
    };
  }

  private final GLFWMouseButtonCallback mouseButtonCallback = new GLFWMouseButtonCallback() {
    @Override
    public void invoke(long window, int button, int action, int mods) {
      if (button == GLFW_MOUSE_BUTTON_LEFT) {
        mouseLeft = action == GLFW_PRESS;
      } else if (button == GLFW_MOUSE_BUTTON_RIGHT) {
        mouseRight = action == GLFW_PRESS;
      }

      if (windowMode.equals("display") && button == GLFW_MOUSE_BUTTON_LEFT) {
        double[] xPos = new double[1];
        double[] yPos = new double[1];
        glfwGetCursorPos(window, xPos, yPos);
        oldMx = xPos[0];
        oldMy = yPos[0];
      } else if (windowMode.equals("input") && button == GLFW_MOUSE_BUTTON_LEFT) {
        // Stop stepping and wait until last step executes, or it will step while adding.
        boolean currentShouldStep = shouldStep;
        shouldStep = false;
        if (currentShouldStep) {
          try {
            Thread.sleep((int) golStepTimer.getDelay() + 100);
          } catch (InterruptedException ignore) {
          }
        }

        float[] cursorPosition = getGolCursorPosition();
        gol.addStructure(golStructures.get(golStructure), (int) cursorPosition[0], (int) cursorPosition[1]);

        shouldStep = currentShouldStep; // Recover stepping
      } else if (windowMode.equals("input") && button == GLFW_MOUSE_BUTTON_RIGHT) {
        float[] cursorPosition = getGolCursorPosition();
        gol.addStructure(GameOfLifeStructures.erase, (int) cursorPosition[0], (int) cursorPosition[1]);
      }
    }
  };

  private final GLFWKeyCallback keyCallback = new GLFWKeyCallback() {
    @Override
    public void invoke(long window, int key, int scancode, int action, int mods) {
      if (action == GLFW_PRESS || action == GLFW_REPEAT) {
        switch (key) {
          case GLFW_KEY_A:
            camera = camera.left(0.1);
            break;
          case GLFW_KEY_D:
            camera = camera.right(0.1);
            break;
          case GLFW_KEY_W:
            camera = camera.forward(0.1);
            break;
          case GLFW_KEY_S:
            camera = camera.backward(0.1);
            break;
          case GLFW_KEY_R:
            camera = camera.up(0.1);
            break;
          case GLFW_KEY_F:
            camera = camera.down(0.1);
            break;
          case GLFW_KEY_T:
            BufferedImage image = GameOfLifeEtc.golBufferToImage(gol.getCurrentGeneration());
            File file = new File("./saved.png");
            try {
              ImageIO.write(image, "png", file);
            } catch (IOException e) {
              e.printStackTrace();
            }
            gol.setCurrentGeneration(GameOfLifeEtc.imageToGolBuffer(image));
            break;
        }
      }
      if (action == GLFW_PRESS) {
        switch (key) {
          case GLFW_KEY_P:
            polygonMode = polygonModes.get((polygonModes.indexOf(polygonMode) + 1) % polygonModes.size());
            break;
          case GLFW_KEY_L:
            useLight = !useLight;
            break;
          case GLFW_KEY_M:
            windowMode = windowModes.get((windowModes.indexOf(windowMode) + 1) % windowModes.size());
            break;
          case GLFW_KEY_SPACE:
            shouldStep = !shouldStep;
            break;
          case GLFW_KEY_B:
            gol.randomize(50);
            break;
          case GLFW_KEY_C:
            gol.reset();
            break;
        }
      }
    }
  };
}
