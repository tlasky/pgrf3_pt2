#version 420
in vec2 texCoord;
in vec3 normal;
in vec3 light;
in vec3 viewDirection;
in vec4 depthTextureCoord;

out vec4 outColor;

uniform int solid;
uniform bool useLight;
uniform vec3 lightPosition;
uniform sampler2D golTexture;
uniform sampler2D depthTexture;

void main() {
    vec4 baseColor = texture(golTexture, texCoord);
    bool isAlive = baseColor.r > 0.5;

    if (solid == 1) {
        baseColor = isAlive ? vec4(0.5, 1, 0.2, 1) : vec4(0.8, 0.4, 0.9, 1);
    } else if (solid == 2) {
        baseColor = isAlive ? vec4(0.8, 0.498039, 0.196078, 1) : vec4(1, 1, 0, 1);
    } else if (solid == 3) {
        baseColor = isAlive ? vec4(0.8, 0.4, 0.5, 1) : vec4(0.6, 0.4, 0.3, 1);
    } else if (solid == 4) {
        baseColor = vec4(1) - baseColor;
    } else if (solid == 5) {
        baseColor = vec4(0.8, 0.498039, 0.196078, 1);
    } else {
        baseColor = isAlive ? vec4(0.623529, 0.623529, 0.372549, 1) : vec4(0.419608, 0.556863, 0.137255, 1);
    }

    if (useLight) {
        bool lightAttenuation = true;
        bool lightCutoff = true;

        vec3 ambient = vec3(0.4);

        float NdotL = max(0.0, dot(normalize(normal), normalize(light)));
        vec3 diffuse = NdotL * vec3(0.7);

        vec3 halfVector = normalize(light + viewDirection);
        float NdotH = max(0.0, dot(normalize(normal), halfVector));
        vec3 specular = vec3(pow(NdotH, 16.0));

        float lightDist = distance(lightPosition, light);
        float constantAttenuation  = 0.01;
        float linearAttenuation    = 0.1;
        float quadraticAttenuation = 0.01;
        float att = 1;

        if (lightAttenuation) {
            att = 1 / (
            constantAttenuation + linearAttenuation *
            lightDist + quadraticAttenuation *
            lightDist * lightDist
            );
        }

        vec3 colorIntensity = ambient + att * (diffuse + specular);

        float zLight = texture(depthTexture, depthTextureCoord.xy).r;
        float zActual = depthTextureCoord.z;

        if (lightCutoff) {
            float headlampCut = 0.976;
            float spotEffect = max(dot(normalize(-lightPosition), -normalize(light)), 0);
            if (spotEffect > 0.01) {
                float blend = clamp((spotEffect - headlampCut) / (1 - headlampCut), 0.0, 1.0);
                colorIntensity = mix(
                ambient,
                colorIntensity,
                blend
                );
            }
        }

        if (zActual > zLight + 0.0001) {
            colorIntensity = ambient;
        }
        outColor = vec4(colorIntensity * baseColor.rgb, 1.0);
    } else {
        outColor = baseColor;
    }
}
