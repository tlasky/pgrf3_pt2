#PGRF3 - Projekt 2
David Tláskal

[Repository](https://bitbucket.org/tlasky/pgrf3_pt2/src/master/)

##Game of life

Ovládání:
- Pohyb    : Klasické ovládání
- Mezerník : Start / Stop GOL
- L        : Zapnutí / vypnutí světla
- P        : Polygone mode
- C        : Vyčistí GOL
- B        : Random GOL
- M        : Display / Input mode
- Input mode + R mouse     : Odstraní buňku
- Input mode + L mouse     : Přidá buňku / strukturu buněk
- Input mode + Mouse wheel : Výběr struktury buněk
